<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 15/05/2018
 * Time: 23:29
 */

namespace App\Helpers;
use App\Company;


class CompanyHelper
{

    public static function getRandomCompanies($passedKilometers){
        $companies = Company::where('freq', '>', 5)->get(); //possible companies
        $companyCounter = count($companies); //count of possible companies
        $counter = 0; //counter for getting companies
        $lokos = []; //object of random companies

        do {
            $randomIDNumber = rand(0, $companyCounter - 1);
            $company = Company::where('id', '=', $randomIDNumber)->first();
            $companyDistance = $company['distance'];
            if ($companyDistance != 0 && $companyDistance != null && $company['freq'] >= 1){
                array_push($lokos, $company);
                $distance = round($company->distance * 2 / 1000);
                $counter += $distance;
            }

        } while ($counter < $passedKilometers);

        return $lokos;
    }


}