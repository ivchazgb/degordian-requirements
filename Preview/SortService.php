<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 17/05/2018
 * Time: 10:11
 */

namespace App\Services;


class SortService
{
    public static function sort($drives) {

        $sorted = collect($drives)->sortBy(function ($col) {
            return $col;
        })->values()->all();

        return $sorted;
    }
}