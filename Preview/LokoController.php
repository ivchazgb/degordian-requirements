<?php

namespace App\Http\Controllers;

use App\Helpers\CompanyHelper;
use App\Helpers\DateHelper;
use App\Services\SortService;
use Illuminate\Http\Request;
use App\Company;
use App\Vehicle;
use App\Loko;
use App\User;
use Carbon\Carbon;

class LokoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $lokos = Loko::all();

        return view('admin.vehicleApp.home', compact('lokos'));

    }

    public function show()
    {
        $lokos = Auth::user()->lokos()->get();
        return view('admin.vehicleApp.home', compact('lokos'));
    }


    /**
     * @return mixed
     */
    public function create()
    {
        $vehicles = Vehicle::all();
        $users = User::all();
        $companies = Company::all();

        return view('admin.vehicleApp.create', compact('vehicles', 'users', 'companies'));
    }

    public function edit($id)
    {
        $vehicles = Vehicle::all();
        $users = User::all();
        $companies = Company::all();
        $loko = Loko::find($id);
        return view('admin.vehicleApp.update', compact('loko', 'vehicles', 'users', 'companies'));


    }

    public function destroy($id)
    {
        Loko::destroy($id);

        return redirect('/admin/loko/home')->with('message', 'Loko is Deleted!');
    }

    /**
     * SHould be store
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createLoko()
    {

        /*validation*/
        $this->validate(request(), [
            'time' => 'required',
            'start_km' => 'required',
            'end_km' => 'required',
            'vehicle_model' => 'required',
            'user_name' => 'required',
            'company_name' => 'required'

        ]);


        $company = Company::where('name', '=', request('company_name'))->first();


        $username = request('user_name');
        $user = User::where('name', '=', $username)->first();


        $vehicleModel = request('vehicle_model');
        $vehicle = Vehicle::where('model', '=', $vehicleModel)->first();

        Loko::create([
            'time' => NOW(),
            'start_km' => request('start_km'),
            'end_km' => request('end_km'),
            'vehicle_id' => $vehicle->id,
            'user_id' => $user->id,
            'company_id' => $company->id,
            'report' => request('report')
        ]);

        $vehicle->km = request('end_km');
        $vehicle->save();

        /*redirect back*/
        return redirect('/admin/loko/home')->with('message', 'Drive is Creted!');
    }

    public function update()
    {
        /*validation*/
        $this->validate(request(), [

            'time' => 'required',
            'start_km' => 'required',
            'end_km' => 'required',
            'vehicle_model' => 'required',
            'user_name' => 'required',
            'company_name' => 'required'

        ]);


        $company = Company::where('name', '=', request('company_name'))->first();


        $username = request('user_name');
        $user = User::where('name', '=', $username)->first();

        $model = request('vehicle_model');
        $vehicle = Vehicle::where('model', '=', $model)->first();

        $lokoID = request('id');
        $loko = Loko::where('id', '=', $lokoID)->first();

        $loko->time = request('time');
        $loko->start_km = request('start_km');
        $loko->end_km = request('end_km');
        $loko->vehicle_id = $vehicle->id;
        $loko->user_id = $user->id;
        $loko->company_id = $company->id;
        $loko->report = request('report');

        $loko->save();


        /*redirect back*/
        return redirect('/admin/loko/home')->with('message', 'Drive is Edited!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function calculate(Request $request)
    {
        /*validation*/
        $this->validate($request, [
            'time' => 'required',
            'start_km' => 'required',
            'vehicle_model' => 'required',
            'user_name' => 'required',
            'passed_km' => 'required|numeric'

        ]);

        $user = User::where('name', '=', $request->post('user_name'))->first();

        $randomCompanies = CompanyHelper::getRandomCompanies($request->passed_km);

        $possibleDates = DateHelper::getPosiblleDates($request->time);

        $countDates = DateHelper::countDates($possibleDates); //sums the number of the left dates

        $drives = Loko::calculateDrives($randomCompanies, $countDates, $possibleDates, $user);

        $sorted = SortService::sort($drives); //sort drives by date

        $vehicle = Vehicle::where('model', '=', request('vehicle_model'))->first(); //requested vehicle
        Loko::makeLokoDrive($sorted, $randomCompanies, $vehicle);

        return redirect('/admin/loko/home')->with('message', 'Calculation Done!');

    }

}
