<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 15/05/2018
 * Time: 23:39
 */

namespace App\Helpers;

use Carbon\Carbon;


class DateHelper
{
    public static function getPosiblleDates($month)
    {

        $start = Carbon::parse($month)->startOfMonth();
        $end = Carbon::parse($month)->endOfMonth();

        $dates = [];


        while ($start->lte($end)) {
            $dates[] = $start->copy();
            $start->addDay();
        }

        //check if the date is weekend and removes it from the $dates
        for ($i = 0; $i < count($dates); $i++) {
            if ($dates[$i]->isWeekend()) {
                unset($dates[$i]);
            }
        }

        $rebasedDates = array_values($dates); //resets the unseted ID-s

        return $rebasedDates;
    }

    public static function countDates($dates)
    {
        return count($dates);
    }



}